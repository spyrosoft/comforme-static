* Pages
** TODO Create About Page with icon
** DONE Add tooltips to the global nav
   CLOSED: [2015-01-11 Sun 11:41]
** DONE Remove mobile view from global nav
   CLOSED: [2015-01-11 Sun 11:13]
** DONE Create Page Page
   CLOSED: [2015-01-11 Sun 10:51]
   - Title textbox
   - Description textarea
** DONE Communities Search Snippet
   CLOSED: [2015-01-10 Sat 21:49]
** DONE Settings Page
   CLOSED: [2015-01-10 Sat 18:30]
   - List user's current communities
   - Communities search
   - Title: "Identify Your Communities"
   - Search textbox
   - Top search results pre-displayed
   - Optimally existing communities will ajax down as the user types
** DONE Search Results Page
   CLOSED: [2015-01-10 Sat 17:17]
   - Title and truncated page content
   - Meta data such as how many comments on the page
** DONE Main Page
   CLOSED: [2015-01-10 Sat 17:00]
   Search textbox and button
** DONE Global Navigation
   CLOSED: [2015-01-10 Sat 17:00]
   - Main Page (home) link
   - Create Page icon
   - Settings icon
   - Log Out icon
** DONE Sign Up/Log In
   CLOSED: [2015-01-10 Sat 17:00]
   Add error messages and styling for invalid email

* General
** TODO Create color scheme
